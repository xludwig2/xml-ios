//
//  DetailViewController.swift
//  projectXml
//
//  Created by Vit Ludwig on 25.04.18.
//  Copyright © 2018 xludwig. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {
    var store: MasterViewController? = nil
    
    @IBOutlet weak var coordinatesLabel: UILabel!
    @IBOutlet weak var loationNameLabel: UITextView!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var lastLocationDistanceLabel: UILabel!
    
    @IBOutlet weak var showOnMapBtn: UIButton!
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let coordinatesLabel = coordinatesLabel {
                let lat = String(format:"%f", detail.lat)
                let lon = String(format:"%f", detail.lon)
                coordinatesLabel.text = lat + ", " + lon
            }
            if let locationName = loationNameLabel {
                locationName.text = detail.locationName
            }
            if let note = noteTextView {
                note.text = detail.note
            }
            if let distanceLabel = lastLocationDistanceLabel {
                var distance = ""
                if (detail.lastLat != 999.9) { //location is first - last location of first location always have this latitude. Not the best solution, but it works :-)
                    let lastLocation = CLLocation(latitude: detail.lastLat, longitude: detail.lastLon)
                    let thisLocation = CLLocation(latitude: detail.lat, longitude: detail.lon)
                    distance = String(format: "%.3f", (thisLocation.distance(from: lastLocation) / 1000))
                }
                distanceLabel.text = "\(distance) km"
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMap"{
            let controller = (segue.destination as! UINavigationController).topViewController as! MapViewController
            if let detail = detailItem {
                controller.mapItem = detail
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: Location? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

