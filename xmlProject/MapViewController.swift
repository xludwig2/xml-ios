//
//  MapViewController.swift
//  xmlProject
//
//  Created by Vit Ludwig on 25.04.18.
//  Copyright © 2018 xludwig. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = mapItem {
            if let map = mapView {
                let currentLocation = CLLocationCoordinate2D(latitude: detail.lat, longitude: detail.lon)
                let lastLocation = CLLocationCoordinate2D(latitude: detail.lastLat, longitude: detail.lastLon)

                let span = MKCoordinateSpanMake(0.002, 0.002)
                let region = MKCoordinateRegionMake(currentLocation, span)
                map.setRegion(region, animated: true)
                map.mapType = .standard
                
                let lastPlacemark = MKPlacemark(coordinate: lastLocation, addressDictionary: nil)
                let currentPlacemark = MKPlacemark(coordinate: currentLocation, addressDictionary: nil)
                
                let lastMapItem = MKMapItem(placemark: lastPlacemark)
                let currentMapItem = MKMapItem(placemark: currentPlacemark)
                
                let currentPoint = MKPointAnnotation()
                let lastPoint = MKPointAnnotation()
                currentPoint.coordinate = currentLocation
                currentPoint.title = "Vase lokace"
                lastPoint.coordinate = lastLocation
                lastPoint.title = "Minula lokace"
                map.addAnnotation(currentPoint)
                map.addAnnotation(lastPoint)
            
                let directionRequest = MKDirectionsRequest()
                directionRequest.source = lastMapItem
                directionRequest.destination = currentMapItem
                directionRequest.transportType = .walking
                
                //set up the route
                let directions = MKDirections(request: directionRequest)
                directions.calculate { (response, error) in
                    guard let directionResonse = response else {
                        if let error = error {
                            print("we have error getting directions==\(error.localizedDescription)")
                        }
                        return
                    }
                    
                    //get route and assign to our route variable
                    let route = directionResonse.routes[0]
                    
                    //add rout to our mapview
                    self.mapView.add(route.polyline, level: .aboveRoads)
                    
                    //setting rect of our mapview to fit the two locations
                    let rect = route.polyline.boundingMapRect
                    self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
                }
                self.mapView.delegate = self
            }
        }
    }
    
    //MARK:- MapKit delegates
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 4.0
        return renderer
    }
    
    var mapItem: Location? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
