//
//  NewLocationViewController.swift
//  projectXml
//
//  Created by Vit Ludwig on 25.04.18.
//  Copyright © 2018 xludwig. All rights reserved.
//
import UIKit
import MapKit
import Foundation

typealias JSONDictionary = [String:Any]

class NewLocationViewController: UIViewController, CLLocationManagerDelegate {

    // lepe na delegata protokolu
    var store: MasterViewController? = nil
    var locationManager: CLLocationManager! = CLLocationManager()
    var myLocation: CLLocation?
    
    @IBOutlet var latTextField: UITextField!
    @IBOutlet var lonTextField: UITextField!
    @IBOutlet weak var placeLabel: UITextView!
    @IBOutlet weak var noteTextView: UITextView!
    
    @IBAction func saveClicked(_ sender: Any) {
//        let lat = (myLocation?.coordinate.latitude)!49.952473
//        let lon = (myLocation?.coordinate.longitude)!15.268312
        let lat = (latTextField.text! as NSString).doubleValue
        let lon = (lonTextField.text! as NSString).doubleValue
        let locationName = placeLabel.text!
        let note = noteTextView.text!
        let last = store?.getLastLocation()

        self.store?.insertNewLocation(locationName: locationName, lat: lat, lon: lon, lastLocation: last!, note: note)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initLocation()
        configureView()

        // Do any additional setup after loading the view.
    }
    func configureView() {
        noteTextView.layer.borderWidth = 1
        noteTextView.layer.borderColor = UIColor.lightGray.cgColor
        noteTextView.layer.cornerRadius = 5
        
        latTextField.addTarget(self, action: #selector(NewLocationViewController.coordDidChange), for: UIControlEvents.editingChanged)
        lonTextField.addTarget(self, action: #selector(NewLocationViewController.coordDidChange), for: UIControlEvents.editingChanged)
    }
    @objc
    func coordDidChange() {
        //load new coordinates from textfield to var
        let newLat = (latTextField.text! as NSString).doubleValue
        let newLon = (lonTextField.text! as NSString).doubleValue
        myLocation = CLLocation(latitude: newLat, longitude: newLon)

        //set label with place details
        self.getAdress { address, error in
            if let a=address, let city=a["City"], let country=a["CountryCode"], let subLocality=a["SubLocality"] as? String {
                let place = "\(subLocality) - \(city), \(country)"
                self.placeLabel.text = place
            }
        }
    }
    
    func initLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        if(CLLocationManager.locationServicesEnabled()) {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            
            let location = locationManager.location! //current location
            self.myLocation = location
            latTextField.text = String(format: "%f", location.coordinate.latitude)
            lonTextField.text = String(format: "%f", location.coordinate.longitude)
            
            self.getAdress { address, error in
                if let a=address, let city=a["City"], let country=a["CountryCode"], let subLocality=a["SubLocality"] as? String {
                    let place = "\(subLocality) - \(city), \(country)"
                    print(place)
                    self.placeLabel.text = place
                }
            }
            
            locationManager.stopUpdatingLocation()
        }
    }
    func getAdress(completion: @escaping (_ address: JSONDictionary?, _ error: Error?) -> ()) {
        let geoCoder = CLGeocoder()
        let lat = (latTextField.text! as NSString).doubleValue
        let lon = (lonTextField.text! as NSString).doubleValue
//        let lat = 49.9499063
//        let lon = 15.267678

        let location = CLLocation(latitude: lat, longitude: lon)
        geoCoder.reverseGeocodeLocation(location) { placemarks, error in
            if let e = error {
                completion(nil, e)
            } else {
                let placeArray = placemarks
                var placeMark: CLPlacemark!
                placeMark = placeArray?[0]
                guard let address = placeMark.addressDictionary as? JSONDictionary else {
                    return
                }
                completion(address, nil)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        locationManager.stopUpdatingLocation()
//        if ((error) != nil)
//        {
//            print(error)
//        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
